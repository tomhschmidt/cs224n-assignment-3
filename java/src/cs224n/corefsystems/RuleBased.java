package cs224n.corefsystems;

import java.util.*;

import cs224n.coref.*;
import cs224n.coref.ClusteredMention;
import cs224n.coref.Document;
import cs224n.coref.Entity;
import cs224n.coref.Mention;
import cs224n.util.Pair;

public class RuleBased implements CoreferenceSystem {

	Map<String, Map<String, Integer>> mentionHeadCounts = new HashMap<String, Map<String, Integer>>();
  Map<String, int[]> mentionGenderCounts = new HashMap<String,int[]>();
  ArrayList<TreeSet<Integer>> clusters;
  Map<String,TreeSet<Integer>> clustersByString;


	@Override
	public void train(Collection<Pair<Document, List<Entity>>> trainingData) {

		for(Pair<Document, List<Entity>> pair : trainingData){
	      Document doc = pair.getFirst();
	      List<Entity> clusters = pair.getSecond();
	      List<Mention> mentions = doc.getMentions();

        // Count Head Words
	      for(Mention m : mentions) {
	      	String mentionString = m.gloss().toLowerCase().trim();
          if (Pronoun.isSomePronoun(mentionString)) continue;
	      	if (!mentionHeadCounts.containsKey(mentionString)) {
	      		mentionHeadCounts.put(mentionString, new HashMap<String, Integer>());
	      	}
	      	Integer curCount = mentionHeadCounts.get(mentionString).get(m.headWord()) == null ? 0 : mentionHeadCounts.get(mentionString).get(m.headWord()) ;
	      	mentionHeadCounts.get(mentionString).put(m.headWord(), curCount + 1);
	      }

	      for(Entity e : clusters){
	        for(Pair<Mention, Mention> mentionPair : e.orderedMentionPairs()){
            Mention firstMention = mentionPair.getFirst();
            String first = firstMention.gloss().toLowerCase().trim();
            if (!Pronoun.isSomePronoun(first)) continue;
            Pronoun p = Pronoun.valueOrNull(first);
            if (p == null) continue;

            // TODO:  Use the whole word or just the headWord?
            Mention second = mentionPair.getSecond();
            String lemma = second.headToken().word().toLowerCase().trim();
            String nouns = second.glossNouns().toLowerCase().trim();

            if (!mentionGenderCounts.containsKey(lemma)) {
              mentionGenderCounts.put(lemma, new int[3]);
            }
            if (!mentionGenderCounts.containsKey(nouns)) {
              mentionGenderCounts.put(nouns, new int[3]);
            }
            int[] counts = mentionGenderCounts.get(lemma);
            if (p.gender == Gender.MALE)
              counts[0]++;
            else if (p.gender == Gender.FEMALE)
              counts[1]++;
            else
              counts[2]++;

            counts = mentionGenderCounts.get(nouns);
            if (p.gender == Gender.MALE)
              counts[0]++;
            else if (p.gender == Gender.FEMALE)
              counts[1]++;
            else
              counts[2]++;
	        }
	      }

	  }

	}


  private List<ClusteredMention> getCoreferences(List<Mention> mentions) {
    // Converts the clusters into the correct format
    List<ClusteredMention> coreferences = new ArrayList<ClusteredMention>();
    for (int i = 0; i < mentions.size(); i++)
      coreferences.add(null);

    TreeSet<Integer> added = new TreeSet<Integer>();
    ArrayList<Integer> addedList = new ArrayList<Integer>();
    for (int i = 0; i < clusters.size(); i++) {
      TreeSet<Integer> set = clusters.get(i);
      if (set == null || set.isEmpty()) continue;
      int first = set.first();
      if (added.contains(first)) continue;
      Mention m = mentions.get(first);
      ClusteredMention cm = m.markSingleton();
      coreferences.set(first,cm);
      added.add(first);
      addedList.add(first);
      for (int index : set) {
        if (added.contains(index)) continue;
        Mention m2 = mentions.get(index);
        coreferences.set(index, m2.markCoreferent(cm.entity));
        added.add(index);
        addedList.add(index);
      }
    }
    // System.out.println(addedList);
    // System.out.println(addedList.size());
    // System.out.println(coreferences.size());
    // System.out.println(coreferences);
    return coreferences;
  }

  private TreeSet<Integer> mergeClusters(int a, int b) {
    TreeSet<Integer> clusterA = clusters.get(a);
    TreeSet<Integer> clusterB = clusters.get(b);
    for (int c : clusterA) {
      clusters.set(c,clusterA);
    }
    if (clusterB == null) {
      clusterA.add(b);
      clusters.set(b,clusterA);
      return clusterA;
    }
    for (int c : clusterB) {
      clusters.set(c,clusterA);
      clusterA.add(c);
    }
    return clusterA;
  }

  private void matchExact(List<Mention> mentions) {
    // Exact Match
    for (int i = 0; i < mentions.size(); i++) {
      Mention m = mentions.get(i);
      String exactString = m.glossExact().toLowerCase().trim();
      String mentionString = m.gloss().trim();
      String nounString = m.glossNouns().trim();
      String nounArtString = m.glossNounsAndArticles().trim();
      boolean foundMatch = false;
      if (Pronoun.isSomePronoun(m.headWord())) {
        TreeSet<Integer> set = new TreeSet<Integer>();
        set.add(i);
        clusters.set(i, set);
        continue;
      }
      if(clustersByString.containsKey(exactString)) {
        TreeSet<Integer> indices = clustersByString.get(exactString);
        indices.add(i);
        clusters.set(i, indices);
      }
      else if (clustersByString.containsKey(mentionString)) {
        TreeSet<Integer> indices = clustersByString.get(mentionString);
        indices.add(i);
        clusters.set(i, indices);
      } else if (clustersByString.containsKey(nounString)) {
        TreeSet<Integer> indices = clustersByString.get(nounString);
        indices.add(i);
        clusters.set(i, indices);
      } else if (clustersByString.containsKey(nounArtString)) {
        TreeSet<Integer> indices = clustersByString.get(nounArtString);
        indices.add(i);
        clusters.set(i, indices);
      } else {
        //System.out.println(mentionString);
        TreeSet<Integer> set = new TreeSet<Integer>();
        set.add(i);
        clusters.set(i, set);
        clustersByString.put(exactString,set);
        clustersByString.put(mentionString, set);
        clustersByString.put(nounString,set);
        clustersByString.put(nounArtString,set);
      }
    }
  }

  private void matchExactNouns(List<Mention> mentions) {
    // Exact Match
    for (int i = 0; i < mentions.size(); i++) {
      Mention m = mentions.get(i);
      String mentionString = m.glossNouns();
      // String mentionString = m.gloss().toLowerCase();
      if(clustersByString.containsKey(mentionString)){
        TreeSet<Integer> indices = clustersByString.get(mentionString);
        indices.add(i);
        clusters.set(i, indices);
      } else {
        //System.out.println(mentionString);
        TreeSet<Integer> set = new TreeSet<Integer>();
        set.add(i);
        clusters.set(i, set);
        clustersByString.put(mentionString, set);
      }
    }
  }

  private void matchAllHeadWords(List<Mention> mentions) {
    for (int i = 0; i < mentions.size(); i++) {
      Mention m = mentions.get(i);
      String headWord = m.headWord();
      if (Pronoun.isSomePronoun(headWord)) {
        continue;
      }
      for (int j = 0; j < i; j++) {
        Mention other = mentions.get(j);
        String otherHead = other.headWord();
        if (headWord.equals(otherHead)) {
          mergeClusters(i,j);
        }
      }
    }
  }

  private void matchHeadsAggressive(List<Mention> mentions) {
    // Head Matching
    for (int i = 0; i < mentions.size(); i++) {
      Mention curMention = mentions.get(i);
      String mentionString = curMention.gloss().toLowerCase().trim();

      if (mentionHeadCounts.containsKey(mentionString)) {
        int topIndex = -1;
        int topOverlap = 0;
        for (int j = 0; j < i; j++) {
          TreeSet<Integer> potentialCoref = clusters.get(i);
          Mention potentialCorefMention = mentions.get(i);
          String potentialCorefString = potentialCorefMention.gloss().toLowerCase().trim();

          int currOverlap = 0;
          if (mentionHeadCounts.containsKey(potentialCorefString)) {
            for (String currMentionHead : mentionHeadCounts.get(mentionString).keySet()) {
              if (mentionHeadCounts.get(potentialCorefString).containsKey(currMentionHead)) {
                currOverlap += Math.min(mentionHeadCounts.get(mentionString).get(currMentionHead), mentionHeadCounts.get(potentialCorefString).get(currMentionHead));
              }
            }
          }
          if (currOverlap > topOverlap) {
            topOverlap = currOverlap;
            topIndex = j;
          }
        }
        if (topOverlap > 0) {
          mergeClusters(topIndex,i);
        }
      }
    }
  }

  private void matchHeads(List<Mention> mentions) {
    // Head Matching
    for (int i = 0; i < mentions.size(); i++) {
      Mention curMention = mentions.get(i);
      String mentionString = curMention.gloss().toLowerCase().trim();

      if(clustersByString.containsKey(mentionString)){
        TreeSet<Integer> indices = clustersByString.get(mentionString);
        indices.add(i);
        clusters.set(i, indices);
        continue;
      } else if (mentionHeadCounts.containsKey(mentionString)) {
        int topIndex = -1;
        int topOverlap = 0;
        for (int j = 0; j < i; j++) {
          TreeSet<Integer> potentialCoref = clusters.get(i);
          Mention potentialCorefMention = mentions.get(i);
          String potentialCorefString = potentialCorefMention.gloss().toLowerCase().trim();

          int currOverlap = 0;
          if (mentionHeadCounts.containsKey(potentialCorefString)) {
            for (String currMentionHead : mentionHeadCounts.get(mentionString).keySet()) {
              if (mentionHeadCounts.get(potentialCorefString).containsKey(currMentionHead)) {
                currOverlap += Math.min(mentionHeadCounts.get(mentionString).get(currMentionHead), mentionHeadCounts.get(potentialCorefString).get(currMentionHead));
              }
            }
          }
          if (currOverlap > topOverlap) {
            topOverlap = currOverlap;
            topIndex = j;
          }
        }
        if (topOverlap > 0) {
          clustersByString.put(mentionString, mergeClusters(topIndex,i));
          continue;
        }
      }
      //System.out.println(mentionString);
      TreeSet<Integer> set = new TreeSet<Integer>();
      set.add(i);
      clusters.set(i, set);
      clustersByString.put(mentionString, set);
    }
  }

  private boolean pronounMatch(String prevString, String mentionString, boolean isQuoted) {
    isQuoted = false;
    Pronoun prev = Pronoun.valueOrNull(prevString);
    Pronoun ment = Pronoun.valueOrNull(mentionString);

    if (prev == ment) return true;
    
    boolean match = (prev != null && ment != null &&
            (prev.gender == ment.gender) &&
            (prev.speaker == ment.speaker || isQuoted) &&
            (prev.plural == ment.plural));
    // System.out.println(prevString + "," + mentionString + ": " + match);
    return match;
  }

  // Returns EITHER as a default
  private Gender getGender(String name) {
    if (Name.isName(name))
      return Name.mostLikelyGender(name);
    Gender gender = Gender.EITHER;
    if (mentionGenderCounts.containsKey(name)) {
      int[] counts = mentionGenderCounts.get(name);
      int degreeMale = counts[0] - counts[2];
      int degreeFemale = counts[1] - counts[2];
      if (degreeMale > degreeFemale && degreeMale > counts[2])
        gender = Gender.MALE;
      else if (degreeFemale > degreeMale && degreeFemale > counts[2])
        gender = Gender.FEMALE;
    }
    return gender;
  }
  /*
   *  Currently matches whether the noun and pronoun share the same plurality and gender
   */

  private static final Pronoun[] PersonSetValues = new Pronoun[] { Pronoun.I, Pronoun.ME, Pronoun.MYSELF, Pronoun.MINE, Pronoun.MY, Pronoun.YOU, Pronoun.YOURSELF, Pronoun.YOURS, Pronoun.YOUR, Pronoun.HE, Pronoun.HIM, Pronoun.HIMSELF, Pronoun.HIS, Pronoun.SHE, Pronoun.HER, Pronoun.HERSELF, Pronoun.HERS };
  private static final Set<Pronoun> personPronouns = new HashSet<Pronoun>(Arrays.asList(PersonSetValues));
  private static final Pronoun[] NonPersonSetValues = new Pronoun[] {Pronoun.IT, Pronoun.ITSELF, Pronoun.ITS};
  private static final Set<Pronoun> nonpersonPronouns = new HashSet<Pronoun>(Arrays.asList(NonPersonSetValues));

  private boolean pronounNounMatch(Pronoun p, Mention prevMention, boolean isQuoted) {
    isQuoted = false;
    if (!isQuoted && p.speaker != Pronoun.Speaker.THIRD_PERSON) return false;
    Sentence.Token prevHead = prevMention.headToken();
    boolean isPerson = false;
    boolean isOrg = false;
    for (Sentence.Token token : prevMention.tokens()) {
      if (token.nerTag().equals("PERSON")) {
        isPerson = true;
        break;
      }
      if (token.nerTag().equals("ORG")) {
        isOrg = true;
        break;
      }
    }
    // System.out.println(prevHead.nerTag());
    // System.out.println(prevHead.posTag());    
    boolean plural = prevHead.isPluralNoun();
    if (isOrg && personPronouns.contains(p)) {return false;}
    if (isPerson && nonpersonPronouns.contains(p)) {return false;}
    if (p.gender == Gender.MALE || p.gender == Gender.FEMALE) {
      Gender gender = Gender.EITHER;
      boolean foundName = false;
      for (String word : prevMention.text()) {
        if (Name.isName(word)) {
          gender = Name.mostLikelyGender(word);
          foundName = true;
          break;
        }
      }
      String lemma = prevHead.word().toLowerCase().trim();
      Gender lemmaGender = getGender(lemma);
      Gender nounGender = getGender(prevMention.glossNouns().trim());
      if (foundName && gender != p.gender || (lemmaGender != p.gender && nounGender != p.gender))
        return false;
      //System.out.println("Gender Match:  " + p + " " + prevHead.lemma());
    }
    return p.plural == plural;
  }

  private void matchPronouns(List<Mention> mentions) {

    for (int i = 1; i < mentions.size(); i++) {
      Mention mention = mentions.get(i);
      String mentionString = mention.gloss().toLowerCase().trim();
      Sentence.Token token = mention.headToken();
      boolean isQuoted = token.isQuoted();

      if (!Pronoun.isSomePronoun(mentionString)) continue;

      Pronoun p = Pronoun.valueOrNull(mentionString);
      if (p == null) continue;
      // if ((p.type == Pronoun.Type.POSESSIVE_PRONOUN || p.type == Pronoun.Type.POSESSIVE_DETERMINER)
      //     && mention.length() != 1) {
      //   System.out.println("actually did something");
      //   continue;
      // }
      
      Sentence sentence = mention.sentence;
      boolean matchedWithNoun = false;
      int prev = i-1;
      while (prev > 0) {
        Mention prevMention = mentions.get(prev);
        Sentence.Token prevToken = prevMention.headToken();
        boolean prevIsQuoted = prevToken.isQuoted();
        if (sentence != prevMention.sentence) break;

        String prevString = prevMention.gloss().toLowerCase().trim();
        if (Pronoun.isSomePronoun(prevString) &&
            pronounMatch(prevString,mentionString, isQuoted != prevIsQuoted)) {
          mergeClusters(i,prev);
          prev--;
          continue;
        }
        if (pronounNounMatch(p, prevMention, isQuoted)) {
          mergeClusters(i,prev);
          matchedWithNoun = true;
          break;
        }
        prev--;
      }

      if (matchedWithNoun) continue;
      int end = prev;
      Sentence prevSentence = mentions.get(prev).sentence;
      // get back to the start of the previous sentence
      while (prev > 0 && mentions.get(prev-1).sentence == prevSentence) {
        prev--;
      }
      for (; prev <= end; prev++) {
        Mention prevMention = mentions.get(prev);
        Sentence.Token prevToken = prevMention.headToken();
        boolean prevIsQuoted = prevToken.isQuoted();
        String prevString = prevMention.gloss().toLowerCase().trim();
        if (Pronoun.isSomePronoun(prevString) &&
            pronounMatch(prevString,mentionString, isQuoted != prevIsQuoted)) {
          mergeClusters(i,prev);
          continue;
        }
        if (pronounNounMatch(p, prevMention, isQuoted)) {
          mergeClusters(i,prev);
          break;
        }
      }
    }
  }

  private void matchAllPronouns(List<Mention> mentions, Document doc) {
    for (int i = 0; i < mentions.size(); i++) {
      Mention m = mentions.get(i);
      boolean isQuoted = false;
      for (Sentence.Token token : m.tokens()) {
        if (token.isQuoted()) {
          isQuoted = true;
          break;
        }
      }
      Sentence sentence = m.sentence;
      int indexOfSentence = doc.indexOfSentence(sentence);
      if (m.length() == 1) {
        String mentionString = m.gloss().trim();
        if (!Pronoun.isSomePronoun(mentionString)) continue;
        Pronoun p = Pronoun.valueOrNull(mentionString);
        if (p == null) continue;
        
        boolean found = false;
        for (int j = i-1; j >= 0; j--) {
          Mention otherMention = mentions.get(j);
          String otherString = otherMention.gloss().trim();
          Sentence otherSentence = otherMention.sentence;
          int otherIndex = doc.indexOfSentence(otherSentence);
          int prox = indexOfSentence - otherIndex;
          if (prox > 2) break;
          if (Pronoun.isSomePronoun(otherString)) {
            if ((/*otherMention.sentence == m.sentence && mentionString.equals(otherString)) || */
                // TODO:  how should quotations be dealt with???
                 pronounMatch(otherString,mentionString,isQuoted))) {
              mergeClusters(i,j);
              continue;
            }
          } else if ((prox == 0 || !found) && prox < 2 && pronounNounMatch(p, otherMention, isQuoted)) {
            mergeClusters(i,j);
            found = true;
            continue;
          }
        }
      }
    }
  }

  private boolean NERMatch(Sentence.Token token, Sentence.Token prevToken) {
    String ner = token.nerTag();
    String prevNer = prevToken.nerTag();
    if (!ner.equals(prevNer)) return false;
    if (ner.equals("PERSON")) {
      return getGender(token.lemma()) == getGender(prevToken.lemma()) &&
             token.isPluralNoun() == prevToken.isPluralNoun() &&
             token.isProperNoun() == prevToken.isProperNoun();
    }
    if (ner.equals("ORG")) {
      return token.isProperNoun() == prevToken.isProperNoun() &&
             token.isPluralNoun() == prevToken.isPluralNoun();
    }
    // DATE == DATE or WOA == WOA probably the same
    return true;
  }

  private void matchNER(List<Mention> mentions) {
    for (int i = 1; i < mentions.size(); i++) {
      Mention mention = mentions.get(i);

      Sentence.Token token = mention.headToken();
      String ner = token.nerTag();

      if (ner.equals("O")) continue;
      
      Sentence sentence = mention.sentence;
      boolean matchedWithNoun = false;
      int prev = i-1;
      while (prev > 0) {
        Mention prevMention = mentions.get(prev);
        if (sentence != prevMention.sentence) break;

        Sentence.Token prevToken = prevMention.headToken();
        String prevNer = token.nerTag();
        if (NERMatch(token, prevToken)) {
          mergeClusters(i,prev);
        }
        prev--;
      }
    }
  }

  private void matchAllNER(List<Mention> mentions) {
    List<HashSet<String>> names = new ArrayList<HashSet<String>>();
    List<HashSet<String>> places = new ArrayList<HashSet<String>>();
    for (int i = 0; i < mentions.size(); i++) {
      Mention m = mentions.get(i);
      HashSet<String> person = new HashSet<String>();
      HashSet<String> place = new HashSet<String>();
      for (Sentence.Token token : m.tokens()) {
        String ner = token.nerTag();
        if (ner.equals("PERSON") && token.isProperNoun()) {
          person.add(token.word());
        } else if (ner.equals("ORG") && token.isProperNoun()) {
          place.add(token.word());
        }
      }
      names.add(person);
      places.add(place);
      Gender gender = getGender(m.glossNouns().toLowerCase().trim());
      for (int j = 0; j < i; j++) {
        HashSet<String> otherPerson = new HashSet<String>(names.get(j));
        HashSet<String> otherPlace = new HashSet<String>(places.get(j));
        otherPerson.retainAll(person);
        otherPlace.retainAll(place);
        Gender otherGender = getGender(mentions.get(j).glossNouns().toLowerCase().trim());
        if ((!otherPerson.isEmpty() && gender == otherGender) || !otherPlace.isEmpty()) {
          mergeClusters(i,j);
        }
      }
    }
  }

  private void matchProper(List<Mention> mentions) {
    for (int i = 1; i < mentions.size(); i++) {
      Mention mention = mentions.get(i);

      Sentence.Token token = mention.headToken();
      if (!token.isProperNoun()) continue;
      
      Sentence sentence = mention.sentence;
      boolean matchedWithNoun = false;
      int prev = i-1;
      while (prev > 0) {
        Mention prevMention = mentions.get(prev);
        if (sentence != prevMention.sentence) break;
        Sentence.Token prevToken = prevMention.headToken();
        if (prevToken.isProperNoun()) {
          mergeClusters(i,prev);
        }
        prev--;
      }
    }
  }

	@Override
	public List<ClusteredMention> runCoreference(Document doc) {
    ArrayList<Mention> mentions = new ArrayList<Mention>(doc.getMentions());
    clusters = new ArrayList<TreeSet<Integer>>();
    clustersByString = new HashMap<String,TreeSet<Integer>>();
    for (int i = 0; i < mentions.size(); i++)
      clusters.add(null);

    // EITHER
    matchExact(mentions);
    matchAllHeadWords(mentions);
    // matchHeadsAggressive(mentions);
    // OR
    // matchHeads(mentions);
    // matchExactNouns(mentions);

    // matchAllPronouns(mentions, doc);
    matchPronouns(mentions);
    // matchAllNER(mentions);
    // matchProper(mentions);

    return getCoreferences(mentions);
	}

}
