package cs224n.corefsystems;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import cs224n.coref.ClusteredMention;
import cs224n.coref.Document;
import cs224n.coref.Entity;
import cs224n.coref.Mention;
import cs224n.util.Pair;

public class BetterBaseline implements CoreferenceSystem {

	Map<String, Map<String, Integer>> mentionHeadCounts = new HashMap<String, Map<String, Integer>>();
	
	public void train(Collection<Pair<Document, List<Entity>>> trainingData) {
	}



	public List<ClusteredMention> runCoreference(Document doc) {
	    List<Mention> mentions = doc.getMentions();
	    List<ClusteredMention> coreferences = new ArrayList<ClusteredMention>();

	    for (int i = 0; i < mentions.size(); i++) {
	    	Mention m = mentions.get(i);
	    	String headWord = m.headWord();
	    	boolean found = false;
	    	for (int j = 0; j < i; j++) {
	    		Mention other = mentions.get(j);
	    		String otherHead = other.headWord();
	    		if (headWord.equals(otherHead)) {
	    			coreferences.add(m.markCoreferent(coreferences.get(j).entity));
	    			found = true;
	    			break;
	    		}
	    	}
	    	if (!found) {
	    		coreferences.add(m.markSingleton());
	    	}
	    }
	    return coreferences;
	}

}
